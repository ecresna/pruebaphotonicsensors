#include <opencv2/highgui.hpp>
#include <iostream>
#include "math.h"

#define TIPOMETODO 1

using namespace cv;
using namespace std;

//declaracion de Funciones
double calculoError(Mat imagenCalc, Mat imagenT );
void metodoSSD(Mat imagenL,Mat imagenR,Mat &disparidadResult,int disparidadMax, int ventana, int numcolumnas, int numfilas);
void metodoCorrelacion(Mat imagenL,Mat imagenR,Mat &disparidadResult,int disparidadMax, int ventana, int numcolumnas, int numfilas);
int calculoMedia(Mat matrizEntrada,int inicioU,int finalU,int inicioV,int finalV);

//Main
int main( int argc, char** argv ) {
  
  //carga imaǵenes
  Mat imageR =imread("/home/efren/Documentos/pruebaphotonicsensors/imagen2.png");
  Mat imageL =imread("/home/efren/Documentos/pruebaphotonicsensors/imagen1.png");
  Mat Imverdadera  =imread("/home/efren/Documentos/pruebaphotonicsensors/imagenVerdadera.png");
  
  //parametros imagenes
  int numfilas=imageR.rows;
  int numcolumnas=imageL.cols;

  //parametros disparidades
  int maxVentanas=25;
  int maxDisparidad=20;
  int ssdMax;
  
  //inicializa matriz disparidades (8bits sin signo 1 canal)
  Mat disparidadUINT=Mat::zeros(numfilas,numcolumnas,CV_8UC1);

  // si no hay imágenes
  if(! imageR.data || ! imageL.data ) {
      std::cout <<  "No se puede abrir imagen" << std::endl ;
      return -1;
    }


  
  //bucle de dos parámetros para hacer pruebas

  for (int disparidad=19;disparidad<maxDisparidad;disparidad+=2) // disaridades
  {
    for (int ventana=2;ventana<maxVentanas;ventana+=2) // ventanas
    {
   
   // llamada a la funcion del metodo de matching correspondiente
    if (TIPOMETODO==0)
    {
      metodoSSD(imageL,imageR,disparidadUINT,disparidad,ventana,numcolumnas,numfilas);
      }
    else{
      metodoCorrelacion(imageL,imageR,disparidadUINT,disparidad,ventana,numcolumnas,numfilas);
    }

    // Reescalado para matriz de 8 bits unsigned 1 canal 
    double min, max;
    Mat disparidadCorr;
    minMaxLoc(disparidadUINT, &min, &max); //calcula maximos y minimos
    
    disparidadUINT.convertTo(disparidadCorr, CV_8U, ((max - min) / 255));
    bitwise_not(disparidadCorr,disparidadCorr);

    // calculo error
    double error = calculoError(disparidadCorr, Imverdadera);

    cout << "Error total: " << error << endl;

    // salvar a disco con los parametros 
    cv::namedWindow("Disparidad", cv::WINDOW_AUTOSIZE);
    string archivo = "Disparidad_" + to_string(disparidad) + "_ventanas_" + to_string(ventana) + "_error_" + to_string(error) + ".png";
    cv::imwrite(archivo.c_str(), disparidadCorr);
    }
  }

  
  return 0;
}

//Funcion de cálculo de Error
// entrada: Mat: imagen calculada,Mat: imagen verdadera para comparar
// salida : double: error calculado
double calculoError(Mat imagenCalc, Mat imagenT )
{
  //valores de las imaǵenes
  int maxcols=imagenCalc.cols;
  int maxrows=imagenCalc.rows;

  //inicializa
  double suma=0;

   //cálculo de la media 
   for (int filas=0;filas<maxrows;filas++)
   {
     for (int cols=0;cols<maxcols;cols++)
     {
       uchar resta=imagenCalc.at<uchar>(filas,cols)-imagenT.at<uchar>(filas,cols);
       double restaDouble=(double)resta;
       double cuadrado=pow(restaDouble,2.0);
       suma += cuadrado;

     }
   }
   
   return sqrt(suma/(maxcols*maxcols));


}

//Método básico de matching SSD
//Entrada. Mat: imagen izquierda,Mat: imagen derecha, &Mat: matriz disparidad que será rellenada,int:disparidad máxima, int: tamaño ventana
// int: columnas imagen, int: filas imagen
//Salida. Vacio. La imagen de disparidades se pasa como referencia. 
// El metodo consiste en comparar ventanas de ambas imágenes usando una función que es dependiente de la disparidad. Se calcula el mínimo de esa función para varias disparidades
// y la disparidad que lo genera, es la que se asigna a la imagen disparidadResult 
void metodoSSD(Mat imagenL,Mat imagenR,Mat &disparidadResult,int disparidadMax, int ventana, int numcolumnas, int numfilas)
{
  //inicialización
  double ssd=0;
  int  suma=0;

   // se recorren las filas y las columnas teniendo en cuenta los márgenes y el tamaño de la ventana 
   for (int x=(ventana/2);x<numcolumnas-(ventana/2);x++)
   {
        for (int y=(ventana/2);y<numfilas-(ventana/2);y++)
        {
          
          int mejorDisparidad=0; //se inicia la disparidad
          int ssdMax=65000; // valor arbitrario de inicio
          
          // se definen las ventanas
          int inicioV=x-(ventana/2)+disparidadMax;
          int finalV=x+(ventana/2)+disparidadMax;
            
          int inicioU=y-(ventana/2);
          int finalU=y+(ventana/2);
          
          for (int d=-disparidadMax;d<disparidadMax;d++)
          {
           
           ssd=0;
           // para el método SSD la función es SUM( (Il-Ir)² )       
            for (int v=inicioV;v<finalV;v++)
            {
                               
              for (int u=inicioU;u<finalU;u++)
              {
              
                int valorL=(int)imagenL.at<cv::Vec3b>(u,v)[0];
                int valorR=(int)imagenR.at<cv::Vec3b>(u,v-d)[0];
                int resta= valorL - valorR;

               
                double restaDouble=(double)resta;
                
                double powdouble =pow(restaDouble,2.0);
                
            
                ssd+=powdouble;
                
          
              }
            }

            //Se busca el mínimo para una disparidad dada
            if (ssd<ssdMax)
            {
              ssdMax=ssd; // cuando se halla, se sustituye por un nuevo máximo
              mejorDisparidad=d; //una nueva disparidad
             
              disparidadResult.at<uchar>(y,x)=(uchar)floor(mejorDisparidad*255/disparidadMax);
              
            }
            
            
          }
          
        }
   }


  
}

//Método de matching CCN
//Entrada. Mat: imagen izquierda,Mat: imagen derecha, &Mat: matriz disparidad que será rellenada,int:disparidad máxima, int: tamaño ventana
// int: columnas imagen, int: filas imagen
//Salida. Vacio. La imagen de disparidades se pasa como referencia. 
// El metodo consiste en comparar ventanas de ambas imágenes usando una función que es dependiente de la disparidad. Se calcula el máximo de esa función para varias disparidades
// y la disparidad que lo genera, es la que se asigna a la imagen disparidadResult 
void metodoCorrelacion(Mat imagenL,Mat imagenR,Mat &disparidadResult,int disparidadMax, int ventana, int numcolumnas, int numfilas)
{
  //inicializacion
  double correlacion1;
  double correlacion2;
  double correlacion3;
  
   // se recorren las filas y las columnas teniendo en cuenta los márgenes y el tamaño de la ventana 
   for (int x=(ventana/2);x<numcolumnas-(ventana/2);x++)
   {
        for (int y=(ventana/2);y<numfilas-(ventana/2);y++)
        {
          
          //inicialización
          int mejorDisparidad=0;
          double correlacionMax=0.83; //valor máximo arbitrario
          
          // se definen las ventanas
          int inicioV=x-(ventana/2)+disparidadMax;
          int finalV=x+(ventana/2)+disparidadMax;
            
           int inicioU=y-(ventana/2);
           int finalU=y+(ventana/2);
          
          // se recorren las disparidades
          for (int d=-disparidadMax;d<disparidadMax;d++)
          {
           
           correlacion1=0;
           correlacion2=0;
           correlacion3=0;

           // para el cálculo de la función se necesitan las intensidades medias de cada ventana   
           int imagenLMedia=calculoMedia(imagenL,inicioU,finalU,inicioV,finalV);
           int imagenRMedia=calculoMedia(imagenR,inicioU,finalU,inicioV,finalV);

             
             // esta funcion es la siguiente: (Il-Ilmedia)*(Ir-Irmedia)/ SQRT( SUM( (Il-Ilmedia)²) *  SUM( (Ir-Irmedia)²)  ) 
                      
            for (int v=inicioV;v<finalV;v++)
            {
                               
              for (int u=inicioU;u<finalU;u++)
              {
                // valores de las imagenes izq y der
                int valorL=(int)imagenL.at<cv::Vec3b>(u,v)[0];
                int valorR=(int)imagenR.at<cv::Vec3b>(u,v-d)[0];          
                                

                double producto1= (valorL-imagenLMedia) * (valorR-imagenRMedia);
               

                double producto2_1=  pow( (double)(valorL-imagenLMedia),2.0)  ;
                double producto2_2=   pow( (double)(valorR-imagenRMedia),2.0) ;
                                                    
                correlacion1+=producto1;
                correlacion2+=producto2_1;
                correlacion3+=producto2_2;
            
              }
            }

            
            double totalCorr=0;
            double productoCorr=correlacion2*correlacion3;
        

            double raizcorrelacion=(sqrt(productoCorr));
            
            // se evita una división por cero
            if (raizcorrelacion!=0){
               totalCorr=correlacion1/(raizcorrelacion);}

            // se halla el maximo
            if (totalCorr>correlacionMax)
            {
              // si se ha encontrado un maximo
              correlacionMax=totalCorr;
              mejorDisparidad=d;
              
              disparidadResult.at<uchar>(y,x)=(uchar)floor(mejorDisparidad*255/disparidadMax);
             
            }
            
            
          }
          
        }
   }


  
}

// funcion que calcula los valores medios en una ventana
// Entrada. Mat: matriz para calcular la media, int: inicio U,int: final U, int: inicio V , int: final V 
// Salida. int valor medio de la instensidad
int calculoMedia(Mat matrizEntrada,int inicioU,int finalU,int inicioV,int finalV)
{

 

  int suma=0;
  int result=0;
  int cont=0;

  for (int filas=inicioU;filas<finalU;filas++)
   {
     for (int cols=inicioV;cols<finalV;cols++)
     {
       uchar valor=matrizEntrada.at<uchar>(filas,cols);
       suma += (int)valor;
       cont++;

     }
   }

  result= floor(suma/(cont));
   return result;
}