#include <opencv2/highgui.hpp>
#include <iostream>
#include "math.h"


using namespace cv;
using namespace std;

int main( int argc, char** argv ) {
  
  
  Mat imageR =imread("/home/efren/Documentos/pruebaphotonicsensors/imagen1.png");
  Mat imageL =imread("/home/efren/Documentos/pruebaphotonicsensors/imagen2.png");
  
  int numfilas=imageR.rows;
  int numcolumnas=imageL.cols;

int maxVentanas=40;
int maxDisparidad=90;
int ssdMax;

  
  //Mat disparidadUINT;
  Mat disparidadUINT=Mat::zeros(numfilas,numcolumnas,CV_8UC1);

  
  if(! imageR.data || ! imageL.data ) {
      std::cout <<  "Could not open or find the image" << std::endl ;
      return -1;
    }


  
  //SSD

for (int disparidad=2;disparidad<maxDisparidad;disparidad++)
{
  for (int ventana=8;ventana<maxVentanas;ventana+=2)
  {
  double ssd=0;
  int  suma=0;

   for (int x=(ventana/2);x<numcolumnas-(ventana/2);x++)
   {
        for (int y=(ventana/2);y<numfilas-(ventana/2);y++)
        {
          
          // cout<<x<<" "<<y<<endl;
          
          int inicioV=x-(ventana/2)+maxDisparidad;
          int finalV=x+(ventana/2)+maxDisparidad;
            
           int inicioU=y-(ventana/2);
           int finalU=y+(ventana/2);
          
          for (int d=0;d<maxDisparidad;d++)
          {
           ssdMax=500000;
           ssd=0;
           //cout<<"d: "<<d<<endl;
            
            for (int v=inicioV;v<finalV;v++)
            {
                               
              for (int u=inicioU;u<finalU;u++)
              {
               // cout<<u<<" "<<v-d<<endl;
                int valorL=(int)imageL.at<cv::Vec2b>(u,v)[0];
                int valorR=(int)imageR.at<cv::Vec2b>(u,v-d)[0];
                int resta= valorL - valorR;

                //cout<<"imagenL1: "<<valorL<<endl;
                //cout<<"imagenR1: "<<valorR<<endl;

          
                //cout<<"suma1: "<<resta<<endl;
                double restaDouble=(double)resta;
                
                //double powdouble =pow(restaDouble,2.0);
                //cout<<"cuadrado:"<<powdouble<<endl;
            
                ssd+=pow(restaDouble,2.0);
                //cout<<"ssd: "<<ssd<<endl;
          
              }
            }

            if (ssd<ssdMax)
            {
              ssdMax=ssd;
              //cout<<d<<endl;
              //cout<<ssdMax<<endl;
              disparidadUINT.at<uchar>(y,x)=d;
              cout<<"añadida disparidad: "<<(int)(disparidadUINT.at<uchar>(y,x))<<endl;

            }
            
            
          }
          
        }
   }

 


 

  

 cv::namedWindow( "Disparidad", cv::WINDOW_AUTOSIZE );
  string archivo="Disparidad_" + to_string(disparidad) +"_ventanas_"+to_string(ventana) +".png";
  cv::imwrite(archivo.c_str(), disparidadUINT );




}
}

  
  return 0;
}